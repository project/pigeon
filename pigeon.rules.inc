<?php

/**
 * Implements hook_rules_action_info().
 */
function pigeon_rules_action_info() {
  $actions['pigeon_send_notification'] = array(
    'label' => t('Send a pigeon notification'),
    'base' => 'pigeon_action_send_notification',
    'parameter' => array(
      'title' => array(
        'label' => t('Title'),
        'type' => 'text',
        'save' => TRUE,
      ),
      'message' => array(
        'label' => t('Message'),
        'type' => 'text',
        'save' => TRUE,
      ),
      'icon' => array(
        'label' => t('Icon'),
        'type' => 'text',
        'save' => TRUE,
      ),
      'url' => array(
        'label' => t('Url'),
        'type' => 'text',
        'save' => TRUE,
      ),
    ),
    'group' => 'pigeon',
    'access callback' => 'pigeon_rules_integration_access',
  );

  return $actions;
}

/**
 * Callback for action execution.
 */
function pigeon_action_send_notification($title, $message, $icon, $url) {
  $result = array(
    'title' => $title,
    'text' => $message,
    'roles' => _pigeon_admin_defaults('roles'),
    'icon' => $icon,
    'url' => $url,
  );

  _pigeon_build_message_variable($result);
}


/**
 * Access callback (use existing permission for module).
 */
function pigeon_rules_integration_access($type, $name) {
  return user_access('administer pigeon');
}
