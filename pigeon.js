/**
 * @file
 * Pigeon javascript functionality for the end-user.
 */

(function ($) {

  'use strict';

  Drupal.behaviors.pigeon = {
    attach(context, settings) {
      Drupal.pigeon.init();
    }
  };

  // @todo: Maybe consider adding an administrative option to enable/disable
  // javascript debugging: instead of commenting out the console.logs we can
  // just enable them from the administrative interface in order to be able to
  // understand where an installation went wrong and is not working.
  Drupal.pigeon = {};

  Drupal.pigeon.init = function () {
    if (!Drupal.pigeon.notification.supported()) {
      // console.log('Notifications not supported by browser!');
      return;
    }

    // If the setting is to inject a button, we only inject it if the user has
    // not accepted the notifications yet (if she has, no need to inject: we're
    // already there). We also don't inject the button if the user denied the
    // notifications (the browser will not allow you to request again)
    if (
      Drupal.pigeon.helpers.getSetting('button') === 'inject' &&
      (!Drupal.pigeon.notification.accepted() ||
        Drupal.pigeon.notification.denied())
    ) {
      Drupal.pigeon.helpers.injectButtonMarkup();
    }

    // We need to do the class swapping if the site admin set the relevant
    // settings
    Drupal.pigeon.helpers.classSwitcheroo();

    // This should always happen.
    // The button should always prompt for a "request permission action".
    Drupal.pigeon.helpers.buttonListener();

    if (!Drupal.pigeon.notification.asked()) {
      // console.log('User has not made a choice about notifications yet');
      // Only send a "request permission" without user gesture if:
      // a) the browser supports it
      // b) the "ask" option is selected in the admin interface
      // Note: "ask" is not the default option so that there are minimal
      //       surprises in the sites that just enabled the module.
      if (
        Drupal.pigeon.helpers.browserSupportsDirectAsk() &&
        Drupal.pigeon.helpers.getSetting('browser') === 'ask'
      ) {
        Drupal.pigeon.notification.ask();
        return;
      }
    }

    if (Drupal.pigeon.notification.accepted()) {
      // console.log('User has accepted the notifications');
      Drupal.pigeon.sse.listen();
    }
  };

  // Notifications
  Drupal.pigeon.notification = {};

  Drupal.pigeon.notification.supported = function () {
    return 'Notification' in window;
  };

  Drupal.pigeon.notification.asked = function () {
    return Notification.permission !== 'default';
  };

  Drupal.pigeon.notification.accepted = function () {
    return Notification.permission === 'granted';
  };

  Drupal.pigeon.notification.denied = function () {
    return Notification.permission === 'denied';
  };

  Drupal.pigeon.notification.ask = function () {
    Notification.requestPermission(permission => {
      if (permission === 'granted') {
        Drupal.pigeon.sse.listen();
      }
    });
  };

  // Server-Side Events
  Drupal.pigeon.sse = {};

  Drupal.pigeon.sse.listen = function () {
    // If the browser does not support localStorage, we're out of luck:
    // no need to even try setting up an EventSource.
    if (!Drupal.pigeon.helpers.supportsLocalStorage()) {
      return;
    }

    let pigeon;
    try {
      let esPath = Drupal.pigeon.helpers.getSetting('esPath');
      pigeon = new EventSource(esPath);
    }
    catch (e) {
      // Easiest way to check if the browser supports EventSource is to try
      // to instantiate it and catch any errors.
      return;
    }
    pigeon.addEventListener('pigeonmsg', Drupal.pigeon.sse.process);
  };

  Drupal.pigeon.sse.process = function (data) {
    const values = JSON.parse(data.data);

    // console.log(values);

    const id = values.id.toString();
    if (!Drupal.pigeon.helpers.shouldBePushed(id)) {
      return;
    }

    const notification = new Notification(values.title, {
      icon: values.icon,
      body: values.text
    });

    notification.onclick = function () {
      window.open(values.url);
    };
  };

  // Helpers
  Drupal.pigeon.helpers = {};

  Drupal.pigeon.helpers.supportsLocalStorage = function () {
    return (
      !!window.localStorage &&
      typeof localStorage.getItem === 'function' &&
      typeof localStorage.setItem === 'function' &&
      typeof localStorage.removeItem === 'function'
    );
  };

  Drupal.pigeon.helpers.hasStoredId = function () {
    return !!localStorage.getItem('pigeon');
  };

  Drupal.pigeon.helpers.shouldBePushed = function (id) {
    // If there is no previous coo sent, or the ids are different,
    // we can safely send it and save it in the localStorage
    if (
      !Drupal.pigeon.helpers.hasStoredId() ||
      localStorage.getItem('pigeon') !== id
    ) {
      localStorage.setItem('pigeon', id);
      return true;
    }

    return false;
  };

  Drupal.pigeon.helpers.getSetting = function (name) {
    if (Drupal.settings.pigeon && Drupal.settings.pigeon[name]) {
      return Drupal.settings.pigeon[name];
    }
    return false;
  };

  /**
   * For now we just check if the browser is Firefox >= 72.
   *
   * However, at some point, all browsers will move towards this trend (aka not
   * to allow notification prompts without an affirmative user gesture).
   *
   * Also, there may be other browsers that have already moved that way.
   *
   * Note that the main concern for browser vendors is that websites will start
   * adding an overlay that would trigger the click no matter what the user
   * does in the site anyway. Mozilla does not seem to care about the specific
   * argument.
   *
   * Related discussion can be found here:
   * https://github.com/wicg/interventions/issues/49
   * (check the last comment for more discussion links)
   *
   * @return {boolean} Whether the browser is Firefox >= 72
   */
  Drupal.pigeon.helpers.browserSupportsDirectAsk = function () {
    const browserInfo = Drupal.pigeon.helpers.getBrowserNameAndVersion();
    try {
      if (
        browserInfo[0].toLowerCase() === 'firefox' &&
        parseInt(browserInfo[1], 10) >= 72
      ) {
        return false;
      }
      return true;
    }
    catch (e) {
      return true;
    }
  };

  // Helper function found here: https://stackoverflow.com/a/5918791
  // but returning the array instead of joining it so that we can parse
  // the name and the version separately.
  Drupal.pigeon.helpers.getBrowserNameAndVersion = function () {
    const ua = navigator.userAgent;
    let tem;
    let M =
      ua.match(
        /(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i
      ) || [];
    if (/trident/i.test(M[1])) {
      tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
      return `IE ${tem[1] || ''}`;
    }
    if (M[1] === 'Chrome') {
      tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
      if (tem != null) {
        return tem
          .slice(1)
          .join(' ')
          .replace('OPR', 'Opera');
      }
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    tem = ua.match(/version\/(\d+)/i);
    if (tem !== null) {
      M.splice(1, 1, tem[1]);
    }
    return M;
  };

  /**
   * Handle the button click by adding an event listener to the body.
   *
   * This is the same as jQuery's event propagation, but using only vanilla
   * javascript. Since D8 will soon remove jQuery as a dependency, we try to be
   * as kind as possible for the people that may try to do the transition.
   *
   * Remember: _"Always code as if the guy who ends up maintaining your code
   *             will be a violent psychopath who knows where you live."_
   */
  Drupal.pigeon.helpers.buttonListener = function () {
    document.body.addEventListener('click', event => {
      if (!event.target.matches('#pigeon-request-permission')) {
        return;
      }
      // If the element is an <a> we need to make sure that the default
      // action is not followed
      event.preventDefault();
      // If the user denied the notifications and the administrator set the url
      // in the relevant configuration field, we need to make sure that we
      // redirect to that url when the button is clicked
      if (Drupal.pigeon.notification.denied()) {
        const durl = Drupal.pigeon.helpers.getSetting('durl');
        if (durl !== '') {
          window.location.href = durl;
        }
      }
      else if (Drupal.pigeon.notification.accepted()) {
        // No need to assign sth to the button if the user already accepted
      }
      else {
        // This is the main case when the notification status is set to default:
        // We ask the user if he wants to accept the notifications.
        Drupal.pigeon.notification.ask();
      }
    });
  };

  /**
   * Add the custom markup for the button.
   */
  Drupal.pigeon.helpers.injectButtonMarkup = function () {
    const outer = document.createElement('div');
    outer.id = 'pigeon-wrapper';

    const inner = document.createElement('div');
    inner.id = 'pigeon-request-permission';
    inner.textContent = Drupal.t('Enable desktop notifications');

    outer.appendChild(inner);
    document.body.appendChild(outer);

    Drupal.pigeon.helpers.markupWrapperEvents();
  };

  /**
   * Do the class swapping if the user already made a choice
   */
  Drupal.pigeon.helpers.classSwitcheroo = function () {
    // ac: Accepted Classes
    const ac = Drupal.pigeon.helpers.getSetting('ac');
    // dc: Denied Classes
    const dc = Drupal.pigeon.helpers.getSetting('dc');

    const pigeonEl = document.getElementById('pigeon-request-permission');

    if (Drupal.pigeon.notification.accepted() && ac !== '' && pigeonEl) {
      pigeonEl.className = ac;
    }

    if (Drupal.pigeon.notification.denied() && dc !== '' && pigeonEl) {
      pigeonEl.className = dc;
    }
  };

  /**
   * We have made a conscious choice to avoid theming the
   * "#pigeon-request-permission" button so that the users can do whatever they
   * want with it.
   *
   * However, when we theme the wrapper element as a button, the user expects
   * the entire button to be clickable (which is not the case if you don't theme
   * specifically the "#pigeon-request-permission" button too).
   *
   * In order to make sure that there are no "dead zones" in the wrapper button,
   * we add an event listener to the wrapper and forward the click to the actual
   * button.
   *
   * Note: This only happens when the button is injected via javascript. The
   *       site admin does not have to create
   */
  Drupal.pigeon.helpers.markupWrapperEvents = function () {
    document.body.addEventListener('click', event => {
      if (!event.target.matches('#pigeon-wrapper')) {
        return;
      }
      document.getElementById('pigeon-request-permission').click();
    });
  };
})(jQuery);
