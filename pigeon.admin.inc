<?php

/**
 * @file
 * Pigeon administration interface.
 *
 * Creates the 2 administration forms for Pigeon (settings and send).
 * Creates the extra options for the node add/edit form.
 *
 * Author: Thomas Minitsios
 */

/**
 * Form generation function.
 *
 * Creates the administration interface configuration form.
 */
function pigeon_admin_form(array $form, array $form_state) {
  $form = array();

  $form['pigeon'] = array(
    '#type' => 'item',
    '#tree' => TRUE,
  );

  $form['pigeon']['help'] = array(
    '#prefix' => '<h3>',
    '#suffix' => '</h3>',
    '#markup' => t(
        'The default configuration requires an affirmative user
      gesture while also requiring you to add the button to your markup.<br>This
      is a design choice so that merely enabling this module does not change
      or break anything in your site.'
    ),
  );

  $form['pigeon']['browser'] = array(
    '#type' => 'radios',
    '#title' => t('Browser Options'),
    '#description' => t(
        'Most browsers will allow you to request permission
      to send notifications without a user pressing anything (user gesture).
      However, going forward, browsers will explicitly disallow this (Firefox
      already does it since version 72). For more information check
      <a href="https://developer.mozilla.org/en-US/docs/Web/API/notification#Specifications"
      target="_blank">this</a> (scroll up and read the note).'
    ),
    '#default_value' => _pigeon_admin_defaults('browser'),
    '#options' => _pigeon_get_setting('browser'),
  );

  $form['pigeon']['button'] = array(
    '#type' => 'radios',
    '#title' => t('Button Options'),
    '#description' => t(
        'If you choose to create your own button, all you have
      to do is give it an id of:<br>
      <em><strong>pigeon-request-permission</strong></em>'
    ),
    '#default_value' => _pigeon_admin_defaults('button'),
    '#options' => _pigeon_get_setting('button'),
  );

  $form['pigeon']['accepted_classes'] = array(
    '#type' => 'textfield',
    '#title' => t('Element classes when user accepted notifications'),
    '#description' => t(
        'Classes to add to the #pigeon-request-permission
      element if the user accepted the notifications.<br><strong>Note:
      </strong> Separate different classes by space. The existing classes will
      be replaced. If you leave this field empty, the classes of the element
      will not be changed.'
    ),
    '#default_value' => _pigeon_admin_defaults('accepted_classes'),
  );

  $form['pigeon']['denied_classes'] = array(
    '#type' => 'textfield',
    '#title' => t('Element classes when user denied notifications'),
    '#description' => t(
        'Classes to add to the #pigeon-request-permission
      element if the user denied the notifications.<br><strong>Note:
      </strong> Separate different classes by space. The existing classes will
      be replaced. If you leave this field empty, the classes of the element
      will not be changed.'
    ),
    '#default_value' => _pigeon_admin_defaults('denied_classes'),
  );

  $form['pigeon']['denied_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Link of element when user denied notifications'),
    '#description' => t(
        'When the user has denied the notifications (either
      actively or by dismissing the popup too many times) you might want the
      notification button click to take the user to a URL where you can explain
      why notifications are a good idea and how to manually enable them.<br>
      That\'s where this option comes into play!<br>If you leave this field
      empty, clicking the button will do nothing.'
    ),
    '#default_value' => _pigeon_admin_defaults('denied_url'),
  );

  $form['pigeon']['help_defaults'] = array(
    '#markup' => '<hr><h2>Defaults:</h2>',
  );

  $form['pigeon']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Default title'),
    '#description' => t('The default title for the desktop notifications'),
    '#default_value' => _pigeon_admin_defaults('title'),
  );

  $form['pigeon']['icon'] = array(
    '#type' => 'textfield',
    '#title' => t('Default icon path for the notifications'),
    '#description' => t('Please provide an absolute url to the image'),
    '#default_value' => _pigeon_admin_defaults('icon'),
  );

  $form['pigeon']['url'] = array(
    '#type' => 'textfield',
    '#title' => t('Default url for the Pigeon form'),
    '#description' => t(
        'Please provide an absolute url to the default link.
      <br><strong>Important note:</strong> '
    ),
    '#default_value' => _pigeon_admin_defaults('url'),
  );

  $form['pigeon']['roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Default roles to Coo to'),
    '#description' => t(
        'Default roles to send coos to.<br>You can override them
      on a per-send basis.<br>However, if you normally send to every user of
      the site, you can just select anonymous and authenticated users here and
      not have to change this option every time you send a new coo.'
    ),
    '#options' => user_roles(),
    '#default_value' => _pigeon_admin_defaults('roles'),
  );

  $form = system_settings_form($form);

  return $form;
}

/**
 * Form generation function.
 *
 * Creates the form to send a custom "Coo".
 */
function pigeon_admin_form_send(array $form, array $form_state) {
  $form = array();

  $form['pigeon_send'] = array(
    '#type' => 'item',
    '#tree' => TRUE,
  );

  $form['pigeon_send']['info'] = array(
    '#markup' => '<p>' . t('<h2>Coo!</h2>') . '</p>',
  );

  $form['pigeon_send']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Coo title'),
    '#description' => t('The title for the notification.'),
    '#default_value' => _pigeon_admin_defaults('title'),
  );

  $form['pigeon_send']['text'] = array(
    '#type' => 'textarea',
    '#title' => t('Coo Message'),
    '#description' => t('Type in the message that you want to coo (no html)'),
  );

  $form['pigeon_send']['roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Roles'),
    '#description' => t(
        'Select the roles to send the coo to. If a user
      has multiple roles, she will receive only one coo'
    ),
    '#options' => user_roles(),
    '#default_value' => _pigeon_admin_defaults('roles'),
  );

  $form['pigeon_send']['icon'] = array(
    '#type' => 'textfield',
    '#title' => t('Coo icon'),
    '#description' => t('The icon for the notification.'),
    '#default_value' => _pigeon_admin_defaults('icon'),
  );

  $form['pigeon_send']['url'] = array(
    '#type' => 'textfield',
    '#title' => t('Coo url'),
    '#description' => t(
        'Where shall the user be directed to if she clicks
      on the notification?'
    ),
    '#default_value' => _pigeon_admin_defaults('url'),
  );

  $form['pigeon_send']['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Send Coo!'),
  );

  return $form;
}

/**
 * Submission callback for the custom send form.
 */
function pigeon_admin_form_send_submit($form, $form_state) {
  $values = $form_state['values']['pigeon_send'];

  _pigeon_build_message_variable($values);

  drupal_set_message(t('Coo sent!'));
}

/**
 * Add more options.
 */
function pigeon_add_node_options(array &$form, array &$form_state) {
  $form['pigeon'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pigeon'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#access' => user_access('administer pigeon'),
    '#group' => 'additional_settings',
    '#tree' => TRUE,
    '#attributes' => array(
      'class' => array('pigeon-form'),
    ),
    '#attached' => array(
      'js' => array(
        drupal_get_path('module', 'pigeon') . '/pigeon-admin.js',
      ),
    ),
  );

  $form['pigeon']['description'] = array(
    '#prefix' => '<div class="description">',
    '#suffix' => '</div>',
    '#markup' => t('Setup desktop notification'),
  );

  $form['pigeon']['enable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Send notification?'),
  );
}
