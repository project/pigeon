CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers


INTRODUCTION
------------

The Pigeon module provides dirt-simple browser/desktop notifications.
It accomplishes that by using Server-Sent events (NO web sockets).

For a full description of the project visit the project page:
https://www.drupal.org/project/pigeon

To submit bug reports and feature suggestions, or to track changes:
https://www.drupal.org/project/issues/pigeon


REQUIREMENTS
------------

 * In order for Pigeon to function properly, the website needs to be served
   through HTTPS. This is a restriction imposed by browsers and there is
   absolutely no way around it.


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See:
https://www.drupal.org/documentation/install/modules-themes/modules-7 for
further information.


CONFIGURATION
-------------

 1. Download, install and enable the module.

 2. Configure the module at: admin/config/services/pigeon. The default options
    have been selected so that the functionality of your site does not change at
    all when you enable pigeon (you have to manually add a button with an id of
    #pigeon-request-permission and the user has to click it in order for pigeon
    to request the notification permission).

 3. If you want to send a custom notification you can do so by visiting:
    admin/config/services/pigeon/send (there should also be a link at the top
    of the main configuration page named: "Coo!").

 4. Users with the "administer pigeon" permission, should also see a checkbox
    in node add/edit forms which they can use to conveniently send notifications
    about newly created content.


TROUBLESHOOTING
---------------

 * If you have any issues during the installation or use of this module, please
   make sure that your site is served through HTTPS.

 * Also, keep an eye on the javascript console which usually prints helpful
   messages by the browser when things don't work (e.g. a message by Chrome
   stating that: _"Notifications permission has been blocked as the user has
   dismissed the permission prompt several times."_).

 * Note, that most browsers do not allow notification prompts in "Incognito",
   so, when you're testing this module, please make sure that you're not trying
   to do it in _Incognito mode_.

 * Make sure that the browser you're testing pigeon in, supports Server-Sent
   Events (e.g. no IE). For a comprehensive table of support check this:
   https://caniuse.com/#feat=eventsource

 * If you have checked all of the above and you are still having issues, please
   create an issue here: https://www.drupal.org/project/issues/pigeon


FAQ
---

 Q: Why "Pigeon"?

 A: When trying to find a name for the project, the "homing pigeon" idea seemed
    fitting enough and I didn't give it much thought after that.

 -

 Q: Why do you insist on using the word "Coo"?

 A: In order to be able to maintain my sanity while developing a new project, I
    stick to using some words that I find funny. Since: "pigeons go coo!", I
    thought that - in this module's context - "coo" and "notification" should be
    interchangeable.

 -

 Q: Do you believe that "birds aren't real"?

 A: Even though I am aware of said website, I would like to _emphasize_ that
    Pigeon is *in no way affiliated* with the specific _"movement"_.
    However, I can't help but find it extremely amusing to consider a
    "robot pigeon" coo-ing messages in people's desktops.

 -

 Q: Why can't I fine-grain notifications sent from the node form?

 A: This is a conscious decision to make sending a coo when creating a new node
    as simple as possible. If you want more control over the coo that you are
    sending, you can always use the custom coo form.
    That being said, if enough people request for the notification options
    to be available in the node add/edit form too, I will obviously do it.

 -

 Q: What options does the node add/edit form use by default?

 A: By default, the options that are being used when sending a notification for
    the creation of a new node are the ones that you can find in the admin
    config form. The 2 options that differ from the defaults are:
    1) The url: it automatically gets the newly created node's url
    2) The title: it automatically gets the newly created node's title

 -

 Q: How often is the server notification stream updated? Why can't I configure
    this?

 A: This is a fair point but I wanted this module to adhere to the KISS
    principles (Keep It Stupid Simple) and adding more options would most likely
    confuse the users. The server notification stream is updated every 5 minutes
    and it should cover the vast majority of cases. However, the issue queue is
    available to anyone for suggestions.


MAINTAINERS
-----------

Current maintainers:
 * Thomas Minitsios (tmin) - https://www.drupal.org/user/2970281

This project has been sponsored by:
 * Lawspot
   Lawspot's mission is to provide openness and transparency to the law, to
   all citizens. Lawspot is Greece's No.1 online hub for all things legal,
   transforming legislation, case law and legal opinions into a dynamic,
   tool-based, searchable dataset. Visit https://www.lawspot.gr for more
   information.
