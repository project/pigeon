/**
 * @file
 * Attaches node-edit behaviors for the Pigeon module.
 */

(function ($) {

  'use strict';

  Drupal.behaviors.pigeonFieldsetSummaries = {
    attach(context) {
      $('fieldset.pigeon-form', context).drupalSetSummary(context =>
        Drupal.t('Send dekstop notification')
      );
    }
  };
})(jQuery);
